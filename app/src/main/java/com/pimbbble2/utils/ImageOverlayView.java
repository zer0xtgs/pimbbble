package com.pimbbble2.utils;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pimbbble2.R;


public class ImageOverlayView extends RelativeLayout {

    private TextView numOfPages;

    public ImageOverlayView(Context context) {
        super(context);
        init();
    }


    public void setNumOfPages(String description) {
        numOfPages.setText(description);
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_image_overlay, this);
        numOfPages = (TextView) view.findViewById(R.id.number_of_pages);
    }
}
