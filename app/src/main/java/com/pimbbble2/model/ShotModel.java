package com.pimbbble2.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShotModel implements Serializable {

    @SerializedName("likes_count")
    private int likesCount;

    @SerializedName("images")
    private Images images;

    @SerializedName("comments_count")
    private int commentsCount;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("animated")
    private boolean animated;

    @SerializedName("id")
    private int id;

    @SerializedName("attachments_url")
    private String attachments_url;

    @SerializedName("attachments_count")
    private int attachments_count;

    @SerializedName("views_count")
    private int viewsCount;

    @SerializedName("title")
    private String title;

    @SerializedName("user")
    private User user;

    @SerializedName("description")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setAttachments_count(int attachments_count) {
        this.attachments_count = attachments_count;
    }

    public int getAttachments_count() {
        return attachments_count;
    }

    public String getAttachments_url() {
        return attachments_url;
    }

    public void setAttachments_url(String attachments_url) {
        this.attachments_url = attachments_url;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public Images getImages() {
        return images;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setAnimated(boolean animated) {
        this.animated = animated;
    }

    public boolean isAnimated() {
        return animated;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return
                "ShotModel{" +
                        "likes_count = '" + likesCount + '\'' +
                        ",images = '" + images + '\'' +
                        ",comments_count = '" + commentsCount + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",animated = '" + animated + '\'' +
                        ",id = '" + id + '\'' +
                        ",views_count = '" + viewsCount + '\'' +
                        ",title = '" + title + '\'' +
                        ",user = '" + user + '\'' +
                        "}";
    }
}