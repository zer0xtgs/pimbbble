package com.pimbbble2.model;

import com.google.gson.annotations.SerializedName;

public class Images {

	@SerializedName("normal")
	private String normal;

	@SerializedName("hidpi")
	private String hidpi;

	@SerializedName("teaser")
	private String teaser;

	public void setNormal(String normal){
		this.normal = normal;
	}

	public String getNormal(){
		return normal;
	}

	public void setHidpi(String hidpi){
		this.hidpi = hidpi;
	}

	public String getHidpi(){
		return hidpi;
	}

	public void setTeaser(String teaser){
		this.teaser = teaser;
	}

	public String getTeaser(){
		return teaser;
	}

	@Override
 	public String toString(){
		return 
			"Images{" + 
			"normal = '" + normal + '\'' + 
			",hidpi = '" + hidpi + '\'' + 
			",teaser = '" + teaser + '\'' + 
			"}";
		}
}