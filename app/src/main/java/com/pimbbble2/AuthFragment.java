package com.pimbbble2;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.pimbbble2.api.Constants;
import com.pimbbble2.api.DribbbleApi;
import com.pimbbble2.api.RetrofitClient;
import com.pimbbble2.model.TokenModel;
import com.tumblr.remember.Remember;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AuthFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.auth_toolbar)
    Toolbar toolbar;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.auth_progress_bar)
    ProgressBar progressBar;

    DribbbleApi dribbbleApi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_auth, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dribbbleApi = RetrofitClient.getAuthApi();
        toolbar.setTitle(R.string.label_auth);
        swipeRefreshLayout.setOnRefreshListener(this);
        progressBar.setProgressTintList(
                ResourcesCompat.getColorStateList(getResources(), R.color.colorAccent, getActivity().getTheme()));

        setupWebView();

    }

    private void setupWebView() {
        webview.clearCache(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new OAuthWebViewClient());
        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                progressBar.setProgress(newProgress);
            }
        });
        webview.loadUrl(Constants.DRIBBBLE_AUTH_URL
                + "authorize?client_id="
                + Constants.DRIBBBLE_CLIENT_ID);
    }

    private class OAuthWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri = Uri.parse(url);
            if (url.contains("?code")) {
                String code = uri.getQueryParameter("code");
                Remember.putString("code", code);

                dribbbleApi.getToken(Constants.DRIBBBLE_CLIENT_ID,
                        Constants.DRIBBBLE_CLIENT_SECRET,
                        code,
                        Constants.DRIBBBLE_CLIENT_REDIRECT_URL)
                        .enqueue(new Callback<TokenModel>() {
                            @Override
                            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                                String accessToken = response.body().access_token;
                                if (!TextUtils.isEmpty(accessToken)) {
                                    Remember.putString("token", accessToken);
                                    Intent intent = new Intent(getContext(), MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onFailure(Call<TokenModel> call, Throwable t) {
                                //todo
                            }
                        });
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        webview.reload();
        swipeRefreshLayout.setRefreshing(false);
    }
}
