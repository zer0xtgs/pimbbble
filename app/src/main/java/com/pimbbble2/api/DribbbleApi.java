package com.pimbbble2.api;

import com.pimbbble2.model.AttachmentModel;
import com.pimbbble2.model.ShotModel;
import com.pimbbble2.model.TokenModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DribbbleApi {

//    https://dribbble.com/oauth/token
//                                     ?client_id=b02b5ca1e60b4823eac008badf4a5354adfbbceda9683cf6d88367303616bbc3
//                                     &client_secret=1ba71424c7b363b72fd991d8ce701c7a47856b15d711c8895f154d9f9ab4c5a9
//                                     &code=7c8f5b97ed7c6b57af180bae0e2167366c432ab1926c9275fae1a8536ff02675

    @FormUrlEncoded
    @POST("oauth/token")
    Call<TokenModel> getToken(@Field("client_id") String client_id,
                              @Field("client_secret") String client_secret,
                              @Field("code") String code,
                              @Field("redirect_uri") String redirect_uri);

//  https://api.dribbble.com/v1/shots/4210300?access_token=bf6edc4d636feb4cbbe66dd5bdaf37d36ef991df277d465bd9fa92b11d5ca1bc

    @GET("shots/{shot_id}")
    Call<ShotModel> getShot(@Path("shot_id") long shot_id,
                            @Query("access_token") String access_token);


//    https://api.dribbble.com/v1/ shots?page=1&per_page=5&access_token=085960880709305d1ee8633dca84c35bba5ca663e0a3b840bbfdcf4e438fcc6c

    @GET("shots/")
    Call<List<ShotModel>> getShotsListPopular(@Query("page") int page_number,
                                              @Query("per_page") int per_page,
                                              @Query("access_token") String access_token);

    @GET("shots/")
    Call<List<ShotModel>> getShotsListMostViewed(@Query("sort") String sort,
                                                 @Query("page") int page_number,
                                                 @Query("per_page") int per_page,
                                                 @Query("access_token") String access_token);

    @GET("shots/")
    Call<List<ShotModel>> getShotsListMostCommented(@Query("sort") String sort,
                                                    @Query("page") int page_number,
                                                    @Query("per_page") int per_page,
                                                    @Query("access_token") String access_token);


    //  https://dribbble.com/shots?sort=views
    //  https://api.dribbble.com/v1/shots?sort=views&access_token=bf6edc4d636feb4cbbe66dd5bdaf37d36ef991df277d465bd9fa92b11d5ca1bc


//    https://api.dribbble.com/v1/ shots/4251304/attachments?access_token=bf6edc4d636feb4cbbe66dd5bdaf37d36ef991df277d465bd9fa92b11d5ca1bc

    @GET("shots/{shot_id}/attachments")
    Call<List<AttachmentModel>> getAttachmentsList(@Path("shot_id") long shot_id,
                                                   @Query("access_token") String access_token);


//    https://api.dribbble.com/v1/shots/4250841/attachments?access_token=bf6edc4d636feb4cbbe66dd5bdaf37d36ef991df277d465bd9fa92b11d5ca1bc
    @GET("https://api.dribbble.com/v1/shots/4250841/attachments?access_token=bf6edc4d636feb4cbbe66dd5bdaf37d36ef991df277d465bd9fa92b11d5ca1bc")
    Call<List<ShotModel>> testAttachment();

}
