package com.pimbbble2.model;

import com.google.gson.annotations.SerializedName;

public class AttachmentModel{

	@SerializedName("id")
	private int id;

	@SerializedName("thumbnail_url")
	private String thumbnailUrl;

	@SerializedName("url")
	private String url;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setThumbnailUrl(String thumbnailUrl){
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getThumbnailUrl(){
		return thumbnailUrl;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	@Override
 	public String toString(){
		return 
			"AttachmentModel{" + 
			"id = '" + id + '\'' + 
			",thumbnail_url = '" + thumbnailUrl + '\'' + 
			",url = '" + url + '\'' + 
			"}";
		}
}