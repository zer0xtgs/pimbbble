package com.pimbbble2;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.ProgressBar;

import com.pimbbble2.api.Constants;
import com.pimbbble2.api.DribbbleApi;
import com.pimbbble2.api.RetrofitClient;
import com.pimbbble2.model.ShotModel;
import com.pimbbble2.utils.PaginationScrollListener;
import com.pimbbble2.utils.Utils;
import com.tumblr.remember.Remember;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ShotsListViewFragment extends Fragment implements RecyclerViewAdapter.RvOnClickListner {

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private int PAGE_PATINATION = 10;

    private String currentType = Constants.POPULAR;

    DribbbleApi dribbbleApi;

    RecyclerViewAdapter recyclerViewAdapter;
    GridLayoutManager layoutManager;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.rv_progress_bar_center)
    ProgressBar progressBarCenter;
    @BindView(R.id.rv_progress_bar_bottom)
    ProgressBar progressBarBottom;
    @BindView(R.id.fragment_toolbar)
    Toolbar toolbar;

    @OnClick({R.id.filter, R.id.exit})
    void setFilter(View view) {

        switch (view.getId()) {
            case R.id.filter:
                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
                bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
                break;
            case R.id.exit:
                Remember.clear();
                CookieManager.getInstance().removeAllCookie();
                AuthFragment authFragment = new AuthFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, authFragment);
                fragmentTransaction.commit();
                break;
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shots_list_view, container, false);
        ButterKnife.bind(this, v);
        currentPage = PAGE_START;
        recyclerView.setHasFixedSize(true);

        recyclerViewAdapter = new RecyclerViewAdapter(getContext());

        layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.setRvOnClickListner(this);
        recyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);

            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        dribbbleApi = RetrofitClient.getDribbbleApi();

        loadFirstPage();

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setTitle("Popular");
    }

    private void loadFirstPage() {
        callListOfShots().enqueue(new Callback<List<ShotModel>>() {
            @Override
            public void onResponse(Call<List<ShotModel>> call, Response<List<ShotModel>> response) {
                List<ShotModel> shotModelList = response.body();
                progressBarCenter.setVisibility(View.GONE);
                recyclerViewAdapter.addAll(shotModelList);
            }

            @Override
            public void onFailure(Call<List<ShotModel>> call, Throwable t) {
                //todo
            }
        });
    }

    private void loadNextPage() {
        progressBarBottom.setVisibility(View.VISIBLE);

        callListOfShots().enqueue(new Callback<List<ShotModel>>() {
            @Override
            public void onResponse(Call<List<ShotModel>> call, Response<List<ShotModel>> response) {
                progressBarBottom.setVisibility(View.GONE);
                isLoading = false;

                List<ShotModel> shotModelList = response.body();
                recyclerViewAdapter.addAll(shotModelList);

            }

            @Override
            public void onFailure(Call<List<ShotModel>> call, Throwable t) {
                //todo
            }
        });

    }

    private Call<List<ShotModel>> callListOfShots() {
        switch (currentType) {
            case Constants.POPULAR:
                return dribbbleApi.getShotsListPopular(currentPage, PAGE_PATINATION, Remember.getString("token", ""));
            case Constants.MOST_VIEWED:
                return dribbbleApi.getShotsListMostViewed(Constants.MOST_VIEWED, currentPage, PAGE_PATINATION, Remember.getString("token", ""));
            case Constants.MOST_COMMENTED:
                return dribbbleApi.getShotsListMostCommented(Constants.MOST_COMMENTED, currentPage, PAGE_PATINATION, Remember.getString("token", ""));
        }
        return null;
    }

    public void onButtonClicked(String type) {
        switch (type) {
            case Constants.POPULAR:
                toolbar.setTitle("Popular");
                currentPage = PAGE_START;
                currentType = Constants.POPULAR;
                recyclerViewAdapter = new RecyclerViewAdapter(getContext());
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerViewAdapter.setRvOnClickListner(this);
                loadFirstPage();
                break;
            case Constants.MOST_VIEWED:
                toolbar.setTitle("Most Viewed");
                currentPage = PAGE_START;
                currentType = Constants.MOST_VIEWED;
                recyclerViewAdapter = new RecyclerViewAdapter(getContext());
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerViewAdapter.setRvOnClickListner(this);
                loadFirstPage();
                break;
            case Constants.MOST_COMMENTED:
                toolbar.setTitle("Most Commented");
                currentPage = PAGE_START;
                currentType = Constants.MOST_COMMENTED;
                recyclerViewAdapter = new RecyclerViewAdapter(getContext());
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerViewAdapter.setRvOnClickListner(this);
                loadFirstPage();
                break;
        }
    }

    @Override
    public void rvOnItemClick(int position, List<ShotModel> rvShotModelList) {
        ShotModel singleShotModel = rvShotModelList.get(position);

        String shotJsonString = Utils.getGsonParser().toJson(singleShotModel);
        Remember.putString("shotInfo", shotJsonString);

        ShotInfoFragment shotInfoFragment = new ShotInfoFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.container, shotInfoFragment);
        fragmentTransaction.commit();
    }
}
