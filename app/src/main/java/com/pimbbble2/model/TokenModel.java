package com.pimbbble2.model;

public class TokenModel {
    public String access_token;

    public TokenModel(String access_token) {
        this.access_token = access_token;
    }
}
