package com.pimbbble2;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.tumblr.remember.Remember;

public class MainActivity extends AppCompatActivity implements BottomSheetFragment.BottomSheetListner {

    ShotsListViewFragment shotsListViewFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Remember.init(this, "com.pimbbble.app");
        Fresco.initialize(this);

        if (!isLogged()) {
            AuthFragment authFragment = new AuthFragment();

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.container, authFragment);
            fragmentTransaction.commit();

        } else {
            shotsListViewFragment = new ShotsListViewFragment();

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.container, shotsListViewFragment);
            fragmentTransaction.commit();
        }

    }

    public boolean isLogged() {
        if (Remember.getString("token", "").equalsIgnoreCase("")) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onButtonClicked(String type) {
        shotsListViewFragment.onButtonClicked(type);
    }
}
