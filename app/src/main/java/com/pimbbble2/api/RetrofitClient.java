package com.pimbbble2.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public static DribbbleApi getAuthApi() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.DRIBBBLE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(DribbbleApi.class);
    }

    public static DribbbleApi getDribbbleApi(){

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.DRIBBBLE_API_1_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        DribbbleApi dribbbleApi = retrofit.create(DribbbleApi.class);
        return dribbbleApi;
    }

}
