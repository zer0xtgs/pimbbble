package com.pimbbble2;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pimbbble2.api.Constants;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private BottomSheetListner listner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);

        TextView tvPopular = v.findViewById(R.id.popular);
        TextView tvMostViewed = v.findViewById(R.id.most_viewed);
        TextView tvMostCommented = v.findViewById(R.id.most_commented);

        tvPopular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onButtonClicked(Constants.POPULAR);
                dismiss();
            }
        });

        tvMostViewed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onButtonClicked(Constants.MOST_VIEWED);
                dismiss();
            }
        });

        tvMostCommented.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onButtonClicked(Constants.MOST_COMMENTED);
                dismiss();
            }
        });

        return v;
    }

    public interface BottomSheetListner {
        void onButtonClicked(String type);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listner = (BottomSheetListner) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement BottomSheetListner");
        }
    }
}
