package com.pimbbble2.utils;

public interface ILoadMore {
    void onLoadMore();
}
