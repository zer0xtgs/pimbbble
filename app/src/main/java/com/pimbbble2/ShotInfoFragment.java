package com.pimbbble2;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pimbbble2.api.DribbbleApi;
import com.pimbbble2.api.RetrofitClient;
import com.pimbbble2.model.AttachmentModel;
import com.pimbbble2.model.ShotModel;
import com.pimbbble2.utils.ImageOverlayView;
import com.pimbbble2.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;
import com.tumblr.remember.Remember;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

public class ShotInfoFragment extends Fragment {

    @BindView(R.id.shot_info_imageview)
    ImageView shotInfoImageView;
    @BindView(R.id.shot_info_progress)
    ProgressBar progressBar;
    @BindView(R.id.shot_likes_count_text)
    TextView shotLikes;
    @BindView(R.id.shot_views_count_text)
    TextView shotViews;
    @BindView(R.id.shot_comments_count_text)
    TextView shotComments;
    @BindView(R.id.shot_desc)
    TextView shotDesc;

    ShotModel resivedShotModel;

    ImageOverlayView overlayView;

    int numberOfPages;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shot_info, container, false);
        ButterKnife.bind(this, v);
        convertJsonStringToShotObject();
        setupView();


        shotInfoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (resivedShotModel.getAttachments_count() == 0) {
                    List<String> attachmentsUrlList = new ArrayList<>();
                    attachmentsUrlList.add(resivedShotModel.getImages().getHidpi());

                    numberOfPages = attachmentsUrlList.size();
                    ImageViewer.Builder builder = new ImageViewer.Builder(getContext(), attachmentsUrlList)
                            .setStartPosition(0);
                    overlayView = new ImageOverlayView(getContext());
                    builder.setOverlayView(overlayView);
                    builder.setImageChangeListener(getImageChangeListener());
                    builder.show();

                } else {
                    DribbbleApi dribbbleApi = RetrofitClient.getDribbbleApi();
                    Call<List<AttachmentModel>> callAttachmentsList = dribbbleApi.getAttachmentsList(resivedShotModel.getId(), Remember.getString("token", ""));
                    callAttachmentsList.enqueue(new retrofit2.Callback<List<AttachmentModel>>() {
                        @Override
                        public void onResponse(Call<List<AttachmentModel>> call, Response<List<AttachmentModel>> response) {
                            List<AttachmentModel> attachmentModelList = response.body();
                            List<String> attachmentsUrlList = new ArrayList<>();
                            attachmentsUrlList.add(resivedShotModel.getImages().getHidpi());
                            for (AttachmentModel index : attachmentModelList) {
                                attachmentsUrlList.add(index.getUrl());
                            }
                            numberOfPages = attachmentsUrlList.size();
                            ImageViewer.Builder builder = new ImageViewer.Builder(getContext(), attachmentsUrlList)
                                    .setStartPosition(0);
                            overlayView = new ImageOverlayView(getContext());
                            builder.setOverlayView(overlayView);
                            builder.setImageChangeListener(getImageChangeListener());
                            builder.show();

                        }

                        @Override
                        public void onFailure(Call<List<AttachmentModel>> call, Throwable t) {

                        }
                    });
                }

            }
        });

        return v;
    }

    private ImageViewer.OnImageChangeListener getImageChangeListener() {
        return new ImageViewer.OnImageChangeListener() {
            @Override
            public void onImageChange(int position) {
                position++;
                overlayView.setNumOfPages(position + "/" + numberOfPages);
            }
        };
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    void convertJsonStringToShotObject() {
        String shotJsonString = Remember.getString("shotInfo", "");
        resivedShotModel = Utils.getGsonParser().fromJson(shotJsonString, ShotModel.class);
    }

    void setupView() {
        Picasso.with(getContext())
                .load(resivedShotModel.getImages().getHidpi())
                .into(shotInfoImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        //todo
                    }
                });
        shotLikes.setText(shotLikes.getContext().getString(R.string.likes_count, resivedShotModel.getLikesCount()));
        shotViews.setText(shotViews.getContext().getString(R.string.views_count, resivedShotModel.getViewsCount()));
        shotComments.setText(shotComments.getContext().getString(R.string.comments_count, resivedShotModel.getCommentsCount()));

        shotDesc.setText(TextUtils.isEmpty(resivedShotModel.getDescription()) ? "" :
                new SpannedString(Html.fromHtml(resivedShotModel.getDescription())));
        shotDesc.setMovementMethod(LinkMovementMethod.getInstance());

    }
}
