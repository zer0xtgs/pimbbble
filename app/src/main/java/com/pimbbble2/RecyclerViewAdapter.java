package com.pimbbble2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pimbbble2.model.ShotModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<ShotModel> rvShotModelList = null;
    private Context context;
    RvOnClickListner rvOnClickListner;

    private boolean isLoadingAdded = false;

    public RecyclerViewAdapter(Context context) {
        this.context = context;
        rvShotModelList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ItemViewHolder(inflater.inflate(R.layout.shot_item, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        getItemCount();
        ShotModel shotModel = rvShotModelList.get(position);


        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.shotTitle.setText(shotModel.getTitle());
        itemViewHolder.progressBar.setVisibility(View.VISIBLE);


        Picasso.with(context)
                .load(shotModel.getImages().getNormal())
                .into(itemViewHolder.shotImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        itemViewHolder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        //todo
                    }
                });

        itemViewHolder.shotAuthor.setText((shotModel.getUser().getName()));
        String date = shotModel.getCreatedAt();
        date = date.substring(0, date.indexOf("T"));
        itemViewHolder.shotDate.setText(date);

    }

    @Override
    public int getItemCount() {
        return rvShotModelList.size();
    }

    //-------------------- HELPERS -----------------------

    void setRvOnClickListner(RvOnClickListner rvOnClickListner) {
        this.rvOnClickListner = rvOnClickListner;
    }

    public void add(ShotModel r) {
        rvShotModelList.add(r);
        notifyItemInserted(rvShotModelList.size() - 1);
    }

    public void addAll(List<ShotModel> shotResults) {
        for (ShotModel result : shotResults) {
            add(result);
        }
    }


    //---------------------VIEW HOLDERS-------------------

    protected class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView shotTitle;
        private ImageView shotImage;
        private TextView shotAuthor;
        private TextView shotDate;
        private ProgressBar progressBar;

        public ItemViewHolder(final View itemView) {
            super(itemView);
            shotTitle = (TextView) itemView.findViewById(R.id.shot_title);
            shotImage = (ImageView) itemView.findViewById(R.id.shot_image);
            shotAuthor = (TextView) itemView.findViewById(R.id.shot_author);
            shotDate = (TextView) itemView.findViewById(R.id.shot_date);
            progressBar = (ProgressBar) itemView.findViewById(R.id.movie_progress);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (rvOnClickListner != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            rvOnClickListner.rvOnItemClick(position, rvShotModelList);
                        }
                    }
                }
            });
        }
    }

    public interface RvOnClickListner {
        void rvOnItemClick(int position, List<ShotModel> shotModelList);
    }
}

